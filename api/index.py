#!/usr/bin/env python
# /api/index.py
# https://zeit.co/docs/runtimes#official-runtimes/python/python-version

from http.server import BaseHTTPRequestHandler
from cowpy import cow

class handler(BaseHTTPRequestHandler):

    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type','text/plain')
        self.end_headers()
        message = cow.Cowacter().milk('Hello from Python from a ZEIT Now Serverless Function!')
        self.wfile.write(message.encode())
        return
